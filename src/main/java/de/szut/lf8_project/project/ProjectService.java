package de.szut.lf8_project.project;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    private final ProjectRepository repository;

    public ProjectService(ProjectRepository repository) {
        this.repository = repository;
    }

    public ProjectModel create(ProjectModel projectModel) {
        return this.repository.save(projectModel);
    }

    public List<ProjectModel> readAll() {
        return this.repository.findAll();
    }

    public ProjectModel readById(long id) {
        Optional<ProjectModel> projectModelOptional = this.repository.findById(id);
        return !projectModelOptional.isEmpty() ? projectModelOptional.get() : null;
    }

    public void delete(ProjectModel projectModel) {
        this.repository.delete(projectModel);
    }
}
