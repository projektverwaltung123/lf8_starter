package de.szut.lf8_project.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class ProjectGetDto {

    private long id;
    private String description;
    private long creatorId;
    private long employeeId;
    private List<Long> responsibleEmployees;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;

}
