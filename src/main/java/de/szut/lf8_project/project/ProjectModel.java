package de.szut.lf8_project.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.*;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import java.util.Date;




@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "project")

public class ProjectModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private long creatorId;
    private long employeeId;
    private String responsibleEmployees;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;

    public ProjectModel(String description, long creatorId, long employeeId, String responsibleEmployees, String comment, Date startDate, Date plannedEndDate, Date endDate) {
        this.description = description;
        this.creatorId = creatorId;
        this.employeeId = employeeId;
        this.responsibleEmployees = responsibleEmployees;
        this.comment = comment;
        this.startDate = startDate;
        this.plannedEndDate = plannedEndDate;
        this.endDate = endDate;
    }

}


