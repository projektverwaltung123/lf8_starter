package de.szut.lf8_project.project.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectCreateDto {

    private String description;
    private long creatorId;
    private long employeeId;
    private List<Long> responsibleEmployees;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date endDate;

    @JsonCreator
    public ProjectCreateDto(String description, Integer creatorId, Integer employeeId, List<Long> responsibleEmployees, String comment, Date startDate, Date plannedEndDate, Date endDate, List<Long> employees) {
        this.description = description;
        this.creatorId = creatorId;
        this.employeeId = employeeId;
        this.responsibleEmployees = responsibleEmployees;
        this.comment = comment;
        this.startDate = startDate;
        this.plannedEndDate = plannedEndDate;
        this.endDate = endDate;
    }
}
