package de.szut.lf8_project.project;

import de.szut.lf8_project.project.dto.ProjectCreateDto;
import de.szut.lf8_project.project.dto.ProjectGetDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectMapper {

    public ProjectGetDto mapToProjectGetDto(ProjectModel projectModel) {
        List<Long> responsibleEmployees = new ArrayList<>();

        if(projectModel.getResponsibleEmployees() != null) {
            String[] ids = projectModel.getResponsibleEmployees().split(";");
            if(ids.length > 0) {
                for(String id : ids) {
                    responsibleEmployees.add(Long.parseLong(id));
                }
            }
        }

        return new ProjectGetDto(projectModel.getId(), projectModel.getDescription(), projectModel.getCreatorId(),
                projectModel.getEmployeeId(), responsibleEmployees, projectModel.getComment(),
                projectModel.getStartDate(), projectModel.getPlannedEndDate(), projectModel.getEndDate());
    }

    public ProjectModel mapToProjectModel(ProjectCreateDto projectCreateDto) {
        String responsibleEmployees = "";
        for(Long id : projectCreateDto.getResponsibleEmployees()) {
            responsibleEmployees += id + ";";
        }
        responsibleEmployees = responsibleEmployees.substring(0, responsibleEmployees.length() - 1);

        ProjectModel projectModel = new ProjectModel();
        projectModel.setDescription(projectCreateDto.getDescription());
        projectModel.setCreatorId(projectCreateDto.getCreatorId());
        projectModel.setEmployeeId(projectCreateDto.getEmployeeId());
        projectModel.setResponsibleEmployees(responsibleEmployees);
        projectModel.setComment(projectCreateDto.getComment());
        projectModel.setStartDate(projectCreateDto.getStartDate());
        projectModel.setPlannedEndDate(projectCreateDto.getPlannedEndDate());
        projectModel.setEndDate(projectCreateDto.getEndDate());
        return projectModel;
    }
}
